** Добро пожаловать на страницу проекта toilet-line.**

Ниже описаны шаги, необходимые для запуска данного проекта:

**Процесс запуска:**

* Необходим grunt, установленный глобально: ``` npm install grunt -g ```;
* Указать адрес REST'a в файле, находящегося в пути, указанном ниже ``` ..\toilet-line\app\components\application-configuration.js ```:
    * Изменить значение константы ``` 'REST_ADDRESS' ```.
* Запустить сервер с помощью выполнения следующей команды: ``` npm start ```, эта команда включает в себя grunt build.


** Welcome to the toilet-line project.**

Steps to run the project can be found below:

**Steps to run the project:**

* Do not forget to install grunt globally: ``` npm install grunt -g ```;
* Specify the address of the REST Server in the following file: ``` ..\toilet-line\app\components\application-configuration.js ```:
    * Change a value of the constant ``` 'REST_ADDRESS' ```.
* Start http-server: ``` npm start ```, this command runs a grunt build as well.