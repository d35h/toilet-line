(function() { 

  'use strict';

  module.exports = function(grunt) {
    grunt.loadNpmTasks('grunt-contrib-jshint');

    grunt.initConfig({
      pkg: grunt.file.readJSON('package.json'),

      jshint: {
        all: [
          'Gruntfile.js', 
          'app/components/*.js',
          'app/app.js'
        ]
      },

      concat: {
        options: {
          separator: ';'
        },
        dist: {
          src: [ 
              'node_modules/angular/angular.js', 
              'app/components/*.js', 
              'app/app.js'
          ],
          dest: 'app/dist/js/<%= pkg.name %>-<%= pkg.version %>.js'
        }
      },

      uglify: {
        options: {
          banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
        },
        dist: {
          files: {
            'app/dist/js/<%= pkg.name %>-<%= pkg.version %>.min.js': ['<%= concat.dist.dest %>']
          }
        }
      },

      clean: {
        src: ['app/dist/']
      },

      cssmin: {
        dist: {
          options: {
            banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
          },
          files: {
            'app/dist/css/style.min.css': ['app/app.css']
          }
        }
      }

    });
    
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-html2js');
    grunt.loadNpmTasks('grunt-karma');

    grunt.registerTask('default', [ 'jshint', 'clean', 'concat', 'uglify', 'cssmin' ]);
  };

})();