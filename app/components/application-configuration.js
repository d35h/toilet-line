(function() {

	'use strict';

	angular
	    .module('myApp.applicationConfiguration', [])
	    .constant('applicationConfiguration', {
	        REST_ADDRESS: 'http://192.168.1.39',
	        DELAY_MILLISECONDS: '5000',
	        GREEN_PIC_HREF: 'pics/GREEN_TITLE.png',
	        RED_PIC_HREF: 'pics/RED_TITLE.png',
	        YELLOW_PIC_HREF: 'pics/YELLOW_TITLE.png',
	        GREEN_LIGHT_VALUE: 0,
	        RED_LIGHT_VALUE: 1,
	        YELLOW_LIGHT_VALUE: 2
	    });

})();