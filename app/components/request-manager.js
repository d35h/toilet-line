(function() {

    'use strict';

    angular
        .module('myApp.services', [])
        .factory('requestManager', requestManager);

    requestManager.$inject = ['$http', 'applicationConfiguration'];

    function requestManager($http, restConf) {
        return {
            getDoorsStatus: getDoorsStatus
        };

        function getDoorsStatus() {
            return $http.get(restConf.REST_ADDRESS);
        }
    }
    
})();