(function() {

    'use strict';

    angular
        .module('myApp.directives', [])
        .directive('trafficLightDirective', trafficLightDirective);

    function trafficLightDirective() {
        var directive = {
            restrict: 'E',
            templateUrl: 'components/templates/traffic-light-directive.html',
            controller: TrafficLightController,
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;
    }

    TrafficLightController.$inject = ['$interval', '$scope', 'requestManager', 'applicationConfiguration'];

    function TrafficLightController($interval, $scope, requestManager, appConf) {
        console.log('--TrafficLightController was loaded---');
        var vm = this;
        vm.yellowLightValue = appConf.YELLOW_LIGHT_VALUE;
        vm.greenLightValue = appConf.GREEN_LIGHT_VALUE;
        vm.redLightValue = appConf.RED_LIGHT_VALUE;

        vm.doorsStatus = vm.yellowLightValue;

        $scope.$watch('vm.doorsStatus', function() {
            if (vm.doorsStatus === vm.greenLightValue) {
                $scope.iconHref = appConf.GREEN_PIC_HREF;
            }

            if (vm.doorsStatus === vm.redLightValue) {
                $scope.iconHref = appConf.RED_PIC_HREF;
            }

            if (vm.doorsStatus === vm.yellowLightValue) {
                $scope.iconHref = appConf.YELLOW_PIC_HREF;
            }
        });

        //Checks the doors status every (appConf.DELAY_MILLISECONDS / 1000) seconds
        $interval(setDoorsStatus, appConf.DELAY_MILLISECONDS);

        function setDoorsStatus() {
            requestManager.getDoorsStatus().then(function(response) {
                //Setting vm.data according to the fetched data
                vm.doorsStatus = response.data.variables.OpenClose;
            }).catch(function(response) {
                //Setting vm.data to 2 because the communcation with 
                //REST service was not successful and we need to get a 
                //yellow light on the traffic light
                //vm.doorsStatus = 2 - represents a yellow light. Please see application-configuration.js
                vm.doorsStatus = appConf.YELLOW_LIGHT_VALUE;
                console.log('Something went wrong, REST response: ', response);
            });
        }
        
    }

})();