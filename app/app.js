(function() {

	'use strict';

	// Declare app level module which depends on views, and components
	angular.module('myApp', [
	  	'myApp.directives',
	  	'myApp.applicationConfiguration',
	  	'myApp.services'
	]);

})();